import random


class Card:
    def __init__(self, suit, rank):
        self.rank = rank
        self.suit = suit

    def tostring(self):
        return f'{self.rank} of {self.suit}'

    def __gt__(self, card2):
        return Card.__getrankval__(self) > Card.__getrankval__(card2)

    def __eq__(self, card2):
        return Card.__getrankval__(self) == Card.__getrankval__(card2)

    def __getrankval__(card):
        if card.rank.isdigit():
            return int(card.rank)
        elif card.rank == 'J':
            return 11
        elif card.rank == 'Q':
            return 12
        elif card.rank == 'K':
            return 13
        elif card.rank == 'A':
            return 14


class Deck:
    def __init__(self):
        self.deck = []
        for suit in ['spades', 'diamonds', 'clubs', 'hearts']:
            for rank in ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']:
                self.deck.append(Card(suit, rank))

    def shuffle(self):
        random.shuffle(self.deck)
        return self.deck

    def tostring(self):
        for card in self.deck:
            card.tostring()

    def deal(self):
        return self.deck.pop()


class Hand:
    def __init__(self, name):
        self.hand = []
        self.name = name

    def tostring(self):
        for card in self.hand:
            print(card.tostring())

    def canplay(self):
        if len(self.hand) >= 1:
            return True
        return False

    def canplaywar(self):
        if len(self.hand) >= 4:
            return True
        return False

    def play(self):
        if self.canplay():
            return self.hand.pop()
        return False

    def shuffle(self):
        random.shuffle(self.hand)
        return self.hand


class Game:
    winner = False

    def __printtotals__(self):
        print(f" P1: {len(self.p1.hand)} P2: {len(self.p2.hand)}")

    def __init__(self, p1name, p2name):
        self.deck = Deck()
        self.deck.shuffle()

        self.p1 = Hand(p1name)
        self.p2 = Hand(p2name)

        self.play()

    def play(self):
        # Deal initial hand
        while len(self.deck.deck) > 0:
            self.p1.hand.append(self.deck.deal())
            self.p2.hand.append(self.deck.deal())

        # While there's no winner
        while self.winner == False:
            # Check if they can play. If not, declare winner and exit loop
            if self.p1.canplay() is False:
                self.winner = self.p2.name
                continue
            if self.p2.canplay() is False:
                self.winner = self.p1.name
                continue

            self.p1.shuffle()
            self.p2.shuffle()
            p1card = self.p1.play()
            p2card = self.p2.play()

            if p1card > p2card:
                self.p1.hand.append(p1card)
                self.p1.hand.append(p2card)
            elif p1card < p2card:
                self.p2.hand.append(p1card)
                self.p2.hand.append(p2card)
            else:
                warwinner = self.__war__()

                if warwinner == 1:
                    self.p1.hand.append(p1card)
                    self.p1.hand.append(p2card)
                else:
                    self.p2.hand.append(p1card)
                    self.p2.hand.append(p2card)

                continue

        return self.winner

    def __war__(self):

        while self.winner == False:
            if self.p1.canplaywar() is False:
                self.winner = self.p2.name
                continue
            if self.p2.canplaywar() is False:
                self.winner = self.p1.name
                continue

            self.p1.shuffle()
            self.p2.shuffle()

            # Deal war chests
            p1cards = []
            p2cards = []
            for i in range(3):
                p1cards.append(self.p1.play())
                p2cards.append(self.p2.play())
            p1card = self.p1.play()
            p2card = self.p2.play()
            if p1card > p2card:
                self.p1.hand.append(p1card)
                self.p1.hand.extend(p1cards)
                self.p1.hand.append(p2card)
                self.p1.hand.extend(p2cards)
                return 1
            elif p1card < p2card:
                self.p2.hand.append(p1card)
                self.p2.hand.extend(p1cards)
                self.p2.hand.append(p2card)
                self.p2.hand.extend(p2cards)
                return 2
            else:

                # War winner = winner of another war. Recuuuuuurrrsssiiooonnn
                warwinner = self.__war__()

                if warwinner == 1:
                    self.p1.hand.append(p1card)
                    self.p1.hand.extend(p1cards)
                    self.p1.hand.append(p2card)
                    self.p1.hand.extend(p2cards)
                elif warwinner == 2:
                    self.p2.hand.append(p1card)
                    self.p2.hand.extend(p1cards)
                    self.p2.hand.append(p2card)
                    self.p2.hand.extend(p2cards)
                return self.__war__()

        return
