import timeit

import war
from matplotlib import pyplot

p1 = "Cooper"
p2 = "Kianna"
numgames = 10

record = {p1: [[0], [0]], p2: [[0], [0]]}

for i in range(numgames):
    start_time = timeit.default_timer()
    winner = war.Game(p1, p2).winner
    elapsed = timeit.default_timer() - start_time
    record[winner][0].append(i)
    record[winner][1].append(elapsed)

p1line = pyplot.plot(record[p1][0], 'ro', label=p1)
p2line = pyplot.plot(record[p2][0], 'bo', label=p2)
pyplot.xlabel("# Won Games")
pyplot.ylabel("# Games")
pyplot.legend()
pyplot.title("# Wins Over " + str(numgames) + " Games Of War")
pyplot.show()

print(record)
